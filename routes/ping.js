const express = require('express');
const router = express.Router();
const pingBackController = require('../controllers/pingBackController');

router.get('/', pingBackController.pingBack);

module.exports = router;



