# Performance-Test-Application

This application helped me to perform automated performance tests on a feature I developed on idealo.de. For my thesis I ran the perf-tests parallel to the AB-Test. The tests were started every 30 minutes between 8:00 and 22:00 for 15 days in a row.

The application was made for my bachelor thesis at idealo.de and Beuth Hochschule Berlin. 

