require('dotenv').config();

/**
 * This object contains the test options and all script instructions.
 */
module.exports.config = {

    script: {
        a: [
            {logData: 0},
            {addHeader: process.env.SITESPEED_HEADER},
            {addHeader: process.env.VARAINT_A_HEADER},
            {setCookie: ['https://www.idealo.de/preisvergleich/ProductCategory/2520.html', 'SSLB=0']},
            {logData: 1},
            {navigate: 'https://www.idealo.de/preisvergleich/ProductCategory/2520.html'},
            {logData: 2},
            {execAndWait: 'document.querySelector(\"[title=In-Ear-Kopfhörer]\").click();'},
        ],

        b: [
            {logData: 0},
            {addHeader: process.env.VARAINT_87_HEADER},
            {addHeader: process.env.SITESPEED_HEADER},
            {addHeader: process.env.VARAINT_B_HEADER},
            {setCookie: ['https://www.idealo.de/preisvergleich/ProductCategory/2520.html', 'SSLB=0']},
            {logData: 1},
            {navigate: 'https://www.idealo.de/preisvergleich/ProductCategory/2520.html'},
            {logData: 2},
            {execAndWait: 'document.querySelector(\"[title=In-Ear-Kopfhörer]\").click();'},
        ],

    },

    options: {

        desktop:
            {
                connectivity: 'Cable',
                firstViewOnly: true,
                location: 'ec2-eu-central-1',
                runs: 1,
                video: true,
                f: 'json',
                netLog: true,
                pingback: '',
            },

        mobile:
            {
                connectivity: '3GFast',
                location: 'ec2-eu-central-1',
                firstViewOnly: true,
                runs: 1,
                repeatView: false,
                video: true,
                f: 'json',
                emulateMobile: true,
                mobile: 1,
                device: "GalaxyS7",
                netLog: true,
                pingback: '',
            },

    }
};

