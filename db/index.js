const assert = require('assert');
const client = require('mongodb').MongoClient;
const config = require('../config/index');
const logger = require('heroku-logger')

let _db;

function initDb(callback) {

    if (_db) {
        logger.info("Trying to init DB again!")
        return callback(null, _db)
    }

    client.connect(config.dbConnectionString, config.dbOptions, connected);

    function connected(error, db) {

        if (error) {
            return callback(error);
        }
        logger.info("DB initialized");
        _db = db;
        return callback(null, _db);
    }
}

function getDb() {
    assert.ok(_db, "Db has not been initialized. Please called init first.");
    return _db;
}

module.exports = {getDb, initDb};
