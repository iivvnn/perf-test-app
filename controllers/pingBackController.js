const getDb = require("../db").getDb;
const WebPageTest = require("../services/WebPageTest").WptStatic;
const logger = require('heroku-logger');
require('dotenv').config();

let db;

/**
 * Recursive Function
 * Replaces dots in key of an object and sets the given id to "_id" to use the orginal WebPageTest-Id for this database
 * @param data - Json Object
 * @returns {Promise | Object} - Object when recursive
 */
normalizeJson = data => {

    if (typeof data !== "object") return data;
    for (let prop in data) {
        if (data.hasOwnProperty(prop)) {
            data[prop.replace(/\./g, "_")] = normalizeJson(data[prop]);
            if (prop.indexOf(".") > -1) {
                delete data[prop];
            }
        }
    }
    return data;
};


/**
 *
 * Replaces dots in key of an object and sets the given id to "_id" to use the orginal WebPageTest-Id for this database
 * @param testId - String
 * @param resultData - Object
 * @param testVariant - Object
 * @returns {Promise}
 */
updateDocument = (testId, resultData, testVariant) => {

    const collection = db.collection(testVariant);
    const query = {"_id": testId};
    const newValue = {$set: resultData};

    return new Promise((resolve, reject) => {
        collection.updateOne(query, newValue, function (error, response) {
            error ? reject(error) : resolve(`WebPageTest results saved! \n More infos: https://www.webpagetest.org/result/${testId}`)
        })
    });

}


module.exports.pingBack = function (req, res) {

    db = getDb().db(process.env.DB_Name);

    const testId = req.query.id ? req.query.id : null;
    const testVariant = req.query.collection ? req.query.collection : null;

    WebPageTest.getWptResults(testId)
        .then(data => normalizeJson(data))
        .then(data => updateDocument(testId, data, testVariant))
        .then(response => {
            logger.info(response);
            res.send(response);
        })
        .catch(error => {
            logger.error(error);
            res.send(`Error Code: ${error.code}`)
        });


};
