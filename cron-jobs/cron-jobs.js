const WptPerformanceTest = require("../services/WebPageTest/index").WptPerformanceTest
const cron = require('node-cron');
const config = require('../test-config/test-config').config;
const logger = require('heroku-logger');


function runAllTests() {

    const scheduling = '*/1 * * * *';
    const scheduleFormatIsValid = cron.validate(scheduling);

    cron.schedule(scheduling, () => {

    logger.info(scheduleFormatIsValid ? "Cron schedule is valid!" : "Cron schedule is not valid!)");

        const mobileA = new WptPerformanceTest("mobile_a", config.script.a, config.options.mobile);
        const mobileB = new WptPerformanceTest("mobile_b", config.script.b, config.options.mobile);

        const desktopA = new WptPerformanceTest("desktop_a", config.script.a, config.options.desktop);
        const desktopB = new WptPerformanceTest("desktop_b", config.script.b, config.options.desktop);

        mobileA.runScript();
        mobileB.runScript();

        desktopA.runScript();
        desktopB.runScript();

    });


}


module.exports.runAllTests = {runAllTests};



