const WebPageTest = require('webpagetest');
const config = require('../../test-config/test-config').config;
const logger = require('heroku-logger');
const getDb = require("../../db").getDb;

/** Class representing a static variant of WebPageTest
 * This class is responsible to fetch the data from WebPageTest.org
 */
class WptStatic {

    /**
     * @param {string} testId
     */
    static getWptResults(testId) {

        const WPT = new WebPageTest('www.webpagetest.org');

        return new Promise((resolve, reject) => {
            WPT.getTestResults(testId,
                (err, data) => (err) ? reject(err) : resolve(data)
            );
        })
    }
}


/** Class representing a way to runScript performance tests with {@link https://www.webpagetest.org | WebPageTest.org }
 */
class WptPerformanceTest {

    /**
     * Create a performance testVariant.
     * @param {Object} testVariant - Create test by setting the test variant (e. g. "mobile_b")
     */
    constructor(testVariant, script, options) {

        this.testVariant = testVariant;
        this.script = script;
        this.options = options;
        this._wpt_url = 'https://www.webpagetest.org';
        this.ping_back_url = "https://wpt-ping.herokuapp.com/ping?collection=";
        this._message = {
            error: {
                no_script: `Script is null or undefined!`,
                no_options: `Options are null or undefined!`,
                no_valid_test_variant: `No valid test variant`,
                no_valid_device: "No valid device!",
                api_key_is_null: "WPT-API-Key is null",
                no_id: "No ID found!"
            },
            success: {
                test_started: "Test started! Waiting for pingback!"
            }
        }
    }


    /**
     * Gets the TestId from the response object
     * @param data
     * @returns {Promise<any>}
     * @private
     */
    _getTestId(data) {

        return new Promise((resolve, reject) => {
            for (let [key, value] of Object.entries(data)) {
                if (key === 'data') {
                    for (let [k, v] of Object.entries(value)) {
                        if (k === 'testId') {
                            resolve(v)
                        } else reject(this._message.error.no_id)
                    }
                }
            }
        })
    }

    /**
     * Checks if daily limit has been reached
     * @param data
     * @returns {Promise<any>}
     * @private
     */
    _hasReachedDailyLimit(data) {
        return new Promise((resolve, reject) => {
            for (let [key, value] of Object.entries(data)) {
                if (key === 'statusCode') {
                    if (value === 400) reject("Daily limit reached");
                    resolve(data);
                }
            }
        })
    }

    /***
     * @param testId - WebPageTest-TestId
     * @returns {Promise<any>}
     * @private
     */
    _insertId(testId) {

        let db = getDb().db(process.env.DB_Name);
        const collection = db.collection(this.testVariant);

        return new Promise((resolve, reject) => {
            collection.insertOne({"_id": testId}, function (error, response) {
                error ? reject(error) : resolve(`Saving was successful (ID: ${testId})`)
            })
        });
    };

    /**
     * @returns {string}
     * @private
     */
    _getWptUrl() {
        return this._wpt_url;
    }

    /**
     *
     * @returns {Promise<any>}
     * @private
     */
    _startTest() {
        return new Promise((resolve, reject) => {

            if (!process.env.WPT_API_KEY) reject(this._message.error.api_key_is_null);
            const wpt = new WebPageTest(this._getWptUrl(), process.env.WPT_API_KEY);
            const options = this.options;
            const script = this.script;
            options.pingback = this.ping_back_url + this.testVariant;
            const scriptToString = WebPageTest.scriptToString(script);

            wpt.runTest(scriptToString, options, (error, data) => {
                if (error) reject(error);
                resolve(data);

            })
        });
    };

    /**
     * - checks if daily tests are reached
     * - gets the ID from the response
     * - saves the ID into the database
     * - loggs success- and error-messages
     */
    runScript() {
        this._startTest()
            .then(data => this._hasReachedDailyLimit(data))
            .then(data => this._getTestId(data))
            .then(testId => this._insertId(testId))
            .then(succesMessage => logger.info(succesMessage))
            .catch(error => logger.error(error));
    }
}


module.exports = {WptPerformanceTest, WptStatic};
