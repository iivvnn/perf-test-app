module.exports = {
    dbConnectionString: require("./config").dbConnectionString,
    dbOptions: require("./config").dbOptions
};
