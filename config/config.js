require('dotenv').config();

const dbConnectionString = process.env.MONGODB_URI;
const dbOptions = {useNewUrlParser: true};

module.exports = {dbConnectionString, dbOptions}
